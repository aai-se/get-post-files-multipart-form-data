package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.google.gson.Gson;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.automationanywhere.commandsdk.model.DataType.STRING;

@BotCommand
@CommandPkg(label="REST POST File", name="REST POST File", description="Sends a file as part of an HTTP POST request", icon="pkg.svg",
        node_label="Classify Document",
        return_type= STRING, return_label="Assign the output to variable", return_required=false)

public class PostFile {

    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

    @Execute
    public Value<String> action(
            @Idx(index = "1", type = AttributeType.TEXT) @Pkg(label = "IQ Bot API endpoint", default_value_type = STRING) @NotEmpty String APIendpoint,
            @Idx(index = "2", type = AttributeType.TEXT) @Pkg(label = "Authorization Token", default_value_type = STRING) @NotEmpty String authToken,
            @Idx(index = "3", type = AttributeType.TEXT) @Pkg(label = "Input File Path", default_value_type = STRING) @NotEmpty String TheFile

    )
    {

        if("".equals(APIendpoint)) {throw new BotCommandException(MESSAGES.getString("emptyInputString", "APIendpoint"));}
        if("".equals(authToken)) {throw new BotCommandException(MESSAGES.getString("emptyInputString", "authToken"));}
        if("".equals(TheFile)) {throw new BotCommandException(MESSAGES.getString("emptyInputString", "InputFilePath"));}

        CloseableHttpClient client0 = HttpClients.createDefault();

        HttpPost uploadFile = new HttpPost(APIendpoint);
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();

        try {
            // This attaches TheFile to the POST:
            File f = new File(TheFile);
            builder.addBinaryBody(
                    "file",
                    new FileInputStream(f),
                    ContentType.MULTIPART_FORM_DATA,
                    f.getName()
            );

        }
        catch (IOException e) {
            return new StringValue(e.getMessage());
        }

        HttpEntity multipart = builder.build();
        uploadFile.setEntity(multipart);
        uploadFile.setHeader("x-authorization",authToken); //set header

        String responseXml1 = "";
        try {
            CloseableHttpResponse response = client0.execute(uploadFile); //execute request
            System.out.println("-----------REQUEST SENT-----------");
            String responseXml = EntityUtils.toString(response.getEntity());
            responseXml1 = responseXml;
            EntityUtils.consume(response.getEntity());
            Map<String, Map<String, String>> javaRootMapObject = new Gson().fromJson(responseXml, Map.class);
            Map<String, String> javaRootMapObject2 = new Gson().fromJson(responseXml, Map.class);
            //status = javaRootMapObject2.get("status");

            System.out.println("Response: " + responseXml);
            System.out.println("javaRootMapObject: " + javaRootMapObject);
        }
        catch (IOException e) {
            System.out.println("-----------REQUEST FAILED-----------");
            return new StringValue(e.getMessage());
        }
        return new StringValue(responseXml1);
    }
}

