package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.google.gson.Gson;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.*;
import java.util.Map;

import static com.automationanywhere.commandsdk.model.DataType.STRING;

@BotCommand
@CommandPkg(label="REST GET File", name="REST GET File", description="Downloads a file returned from an HTTP GET request", icon="pkg.svg",
        node_label="Classify Document",
        return_type= STRING, return_label="Assign the output to variable", return_required=false)

public class DownloadFile {

    private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

    @Execute
    public Value<String> action(
            @Idx(index = "1", type = AttributeType.TEXT) @Pkg(label = "IQ Bot API endpoint", default_value_type = STRING) @NotEmpty String APIendpoint,
            @Idx(index = "2", type = AttributeType.TEXT) @Pkg(label = "Authorization Token", default_value_type = STRING) @NotEmpty String authToken,
            @Idx(index = "3", type = AttributeType.TEXT) @Pkg(label = "Path to Output Folder", default_value_type = STRING) @NotEmpty String folderPath
    )
    {

        if("".equals(APIendpoint)) {throw new BotCommandException(MESSAGES.getString("emptyInputString", "APIendpoint"));}
        if("".equals(authToken)) {throw new BotCommandException(MESSAGES.getString("emptyInputString", "authToken"));}
        if("".equals(folderPath)) {throw new BotCommandException(MESSAGES.getString("emptyInputString", "OnputFilePath"));}

        CloseableHttpClient client0 = HttpClients.createDefault();

        HttpGet downloadFile = new HttpGet(APIendpoint);
        downloadFile.setHeader("x-authorization",authToken);

        // Call API to download file and write to file system
        String trimResponse = "";
        try {
            HttpResponse response = client0.execute(downloadFile);
            System.out.println("-----------REQUEST SENT-----------");
            HttpEntity entity = response.getEntity();

            //make a copy of response entity, since it can only be read once
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            response.getEntity().writeTo(baos);
            byte[] bytes = baos.toByteArray();
            ByteArrayEntity entityCopy = new ByteArrayEntity(bytes);

            //gets the name of the .iqba file from entityCopy
            String responseXml = EntityUtils.toString(entityCopy); //entity consumed once here
            String[] splitResponse = responseXml.split("\\\\");
            trimResponse = splitResponse[0].substring(30).trim();
            System.out.println("Response: " + trimResponse);

            if (entity != null) {
                System.out.println("-----------ENTITY NOT NULL-----------");
                //write response entity stream to file
                InputStream is = entityCopy.getContent();
                String filePath = folderPath + "\\" + trimResponse + ".iqba";
                FileOutputStream fos = new FileOutputStream(new File(filePath));
                int inByte;
                while ((inByte = is.read()) != -1) //attempted to read from closed stream
                    fos.write(inByte);
                is.close();
                fos.close();
            }
        }
        catch (IOException e) {
            System.out.println("-----------Exception: " + e.getMessage() +"-----------");
            return new StringValue(e.getMessage());
        }
        return new StringValue(trimResponse); //modified to return only label
    }
}

