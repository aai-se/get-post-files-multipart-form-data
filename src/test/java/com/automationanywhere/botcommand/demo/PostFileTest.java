package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.Value;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@Test(enabled=true)
public class PostFileTest {

    PostFile command = new PostFile();

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){

        return new Object[][]{
               {"http://ec2-54-188-160-149.us-west-2.compute.amazonaws.com/IQBot/gateway/backups","eyJhbGciOiJSUzUxMiJ9.eyJzdWIiOiI0IiwiY2xpZW50VHlwZSI6IldFQiIsImxpY2Vuc2VzIjpbIkRFVkVMT1BNRU5UIl0sImFuYWx5dGljc0xpY2Vuc2VzUHVyY2hhc2VkIjp7IkFuYWx5dGljc0NsaWVudCI6dHJ1ZSwiQW5hbHl0aWNzQVBJIjp0cnVlfSwidGVuYW50VXVpZCI6IjAwMDAwMDAwLTAwMDAtMDAwMC0wMDAwLTAwMDAwMDAwMDAwMCIsImh5YnJpZFRlbmFudCI6IiIsImlhdCI6MTU4ODM2NDY2MCwiZXhwIjoxNTg4MzY1ODYwLCJpc3MiOiJBdXRvbWF0aW9uQW55d2hlcmUiLCJuYW5vVGltZSI6MjA3NTM2ODY3MjA3NzkwMH0.L7aun91mu6yEBoVZcRszL1cjk_Gg5iWgeY9EwbBUv65bYT9OWGykwz3PxYCYY9joSVSzznw3u3fAz2GlyCPNhyDOc23SbRU8U9PcxhaoKItbq6dzxTAghTzjru0h2BYTAepsObsSRBXa3_mDjJph5NaHciOOIBpdu2Ml0ogVHRkwRdWXBWc-KtbASo8AWTKVShFwB8FGVyGbWEsONzneZ3aMuria6HmhL1W6FrS7brCIZrI3M_dtQWHzpUhwQzJ88tIDunDyilZZtENkdYXrAIPg26KYf8OIvaWemtHwram006hWD3EaB_1cLEZtfPfZ8P10unJJAabupOhLafOVQQ","C:\\Users\\demo\\Downloads\\Learning_Instance_Backup_Demo_20200324_041136.iqba",""}
        };
    }

    @Test(dataProvider = "data")
    public void aTests(String APIendpoint, String authToken, String TheFile, String Results){
        Value<String> d = command.action(APIendpoint, authToken, TheFile);
        String myList = d.get();
        System.out.println("DEBUG RES:"+myList);
        assertEquals(myList,Results );
        //ArrayList<String> ExpectedRes = new ArrayList<String>();
        //ArrayList<String> ActualRes = new ArrayList<String>();

    }
}
