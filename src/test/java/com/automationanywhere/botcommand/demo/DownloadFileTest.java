package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.Value;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@Test(enabled=true)
public class DownloadFileTest {


    DownloadFile command = new DownloadFile();

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){

        return new Object[][]{
               {"http://ec2-54-188-160-149.us-west-2.compute.amazonaws.com/IQBot/gateway/backups/0e4e0ec3-68a1-4196-af13-fd0823d4a4c9/download",
                       "eyJhbGciOiJSUzUxMiJ9.eyJzdWIiOiI0IiwiY2xpZW50VHlwZSI6IldFQiIsImxpY2Vuc2VzIjpbIkRFVkVMT1BNRU5UIl0sImFuYWx5dGljc0xpY2Vuc2VzUHVyY2hhc2VkIjp7IkFuYWx5dGljc0NsaWVudCI6dHJ1ZSwiQW5hbHl0aWNzQVBJIjp0cnVlfSwidGVuYW50VXVpZCI6IjAwMDAwMDAwLTAwMDAtMDAwMC0wMDAwLTAwMDAwMDAwMDAwMCIsImh5YnJpZFRlbmFudCI6IiIsImlhdCI6MTU4ODg5NTA1MSwiZXhwIjoxNTg4ODk2MjUxLCJpc3MiOiJBdXRvbWF0aW9uQW55d2hlcmUiLCJuYW5vVGltZSI6MjYwNTc1ODg3MzUzMTAwMH0.LxW29xxsRrLvJw8cEDBS22TtVlnyHcdHjXxzbO-6FmilsU703ADRihKlGwLsypnk8BdUsxchUaaMA4dPfz0rwQxTr_QKwyXxk1VxsAkk22ETC9WZDTnUaMixOXCkX2fOL7aR9vRiIPyoEL5hHzy7K8POk12x5UR55cqxmUQIYHBg5nsnMgnW3K9tF_EDWmi_F1BHCecqo52Q1IgsQELLnEQ4b9GcU91SDClZqcMX6yJpZGNpf7baRCeJt55KHdJ0sgaGnOyOsXL0qklek9Kx8PPeEi_5yFcg5cqLZVQF3_Zo3upWDfAG5FWD9uE_d8F2Bb0ltUZw0sIYBpbKunoNEA",
                       "C:\\Users\\demo\\Downloads","Learning_Instance_Backup_Demo_20200324_041136"}
                };
    }

    @Test(dataProvider = "data")
    public void aTests(String APIendpoint, String authToken, String folderPath, String Results){
        Value<String> d = command.action(APIendpoint, authToken, folderPath);
        String myList = d.get();
        System.out.println("DEBUG RES:"+myList);
        assertEquals(myList,Results );
        //ArrayList<String> ExpectedRes = new ArrayList<String>();
        //ArrayList<String> ActualRes = new ArrayList<String>();

    }
}
